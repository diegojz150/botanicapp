class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uuid
      t.string :name
      t.string :username
      t.string :email
      t.string :image
      t.string :gender
      t.string :password
      t.boolean :verified
      t.string :provider_uid
      t.integer :role
      t.integer :estado
      t.string :forgot
      
      t.timestamps null: false
    end
  end
  
end
