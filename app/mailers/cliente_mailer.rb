class ClienteMailer < ApplicationMailer
	default from: 'sys@mbotanicapp.com'

	def email_verification(username, email, url)
		@name = username
		@url = url
		@email = email
		mail(to: @email, subject: "Bienvenido")
	end

	def email_forgot(username, email, url)
		@name = username
		@url = url
		@email = email
		mail(to: @email, subject: "Recuperación de contraseña")
	end
end
